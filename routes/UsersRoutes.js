'use strict';
const Controllers = require('../controllers');
const joi = require('joi');

const routes = [{
  method:'GET',
  path:'/users',
  config:{
    handler: function(request,response){
      Controllers.userController.findUsers(request,function(err,data){
        if(err){
          response({
            status:400,
            message:"Error: "+error
          });
        } else {
          response({
            status:200,
            message:"OK",
            data:data
          });
        }
      });
    }
  }
},
{
  method:'GET',
  path:'/user/{user_id}',
  config:{
    handler: function(request, response){
      Controllers.userController.findUser(request,function(err,data){
        if(err){
          response({
            status:400,
            message:"Error: "+err
          });
        } else {
          response({
            status:200,
            message:'OK',
            data:data
          });
        }
      });
    }
  }
},
{
  method: 'POST',
  path: '/add-user',
  config : {
    handler: function(request,response){
      Controllers.userController.addUser(request.payload,function(err,data){
        if(err){
          response({
            status:400,
            message:error
          });
        }else{
          response({
            status:200,
            message:"OK",
            data:data
          });
        }
      })
    },
    validate: {
      payload:{
        type_id : joi.string().required(),
        first_name : joi.string().required(),
        last_name : joi.string().required(),
        email : joi.string().required(),
        password : joi.string().required(),
        address : joi.string().required(),
        city :  joi.string().required(),
        state :  joi.string().required(),
        country : joi.string().required(),
        zip :  joi.string().required(),
        contact :  joi.string().required(),
      }
    }
  }
},
{
  method: 'PUT',
  path: '/update-user',
  config : {
    handler: function(request,response){
      Controllers.userController.updateUser(request.payload,function(err,data){
        if(err){
          response({
            status:400,
            message:error
          });
        }else{
          response({
            status:200,
            message:"User updated successfully.",
            data:response
          });
        }
      })
    },
    validate: {
      payload:{
        user_id : joi.number().required(),
        first_name : joi.string().required(),
        last_name : joi.string().required(),
        password : joi.string().required(),
        address : joi.string().required(),
        city :  joi.string().required(),
        state :  joi.string().required(),
        country : joi.string().required(),
        zip :  joi.string().required(),
        contact :  joi.string().required(),
      }
    }
  }
},
{
  method:'DELETE',
  path:'/delete-user/{user_id}',
  config:{
    handler: function(request, response){
      Controllers.userController.deleteUser(request,function(err,data){
        if(err){
          response({
            status:400,
            message:"Error: "+err
          });
        } else {
          response({
            status:200,
            message:'OK',
            data:data
          });
        }
      });
    }
  }
},
];
 
module.exports = [].concat(routes);