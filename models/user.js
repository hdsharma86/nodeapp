"use strict";

var _ = require('underscore');
var Joi = require('joi');
var crypto = require('crypto');

function UserModel(){
	this.schema = {
		id: Joi.string().max(255),
		type_id: Joi.string().max(255),
		first_name: Joi.string().max(255),
		last_name: Joi.string().max(255),
		email: Joi.string().max(255),
		password: Joi.string().max(255),
		address: Joi.string().max(255),
		city: Joi.string().max(255),
		state: Joi.string().max(255),
		country: Joi.string().max(255),
		zip: Joi.string().max(255),
		contact: Joi.string().max(255),
		last_modified: Joi.string().max(255),
		created_on: Joi.string().max(255)
	};
};

UserModel.prototype = (function() {
	return {
		encryptPass: function(password) {
			var salt = '1d098an18da7cn';
			var sha1 = crypto.createHash('sha1').update(password).digest('hex') + salt;
			return crypto.createHash('sha256').update(sha1).digest('hex');
		}
	};
})();

module.exports = UserModel;