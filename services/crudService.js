"use strict";

var db = require('../config/db');

/**
 * 
 * @param {*} params 
 * @param {*} callback 
 */
const find = function(params, table, callback){
	var sqlQuery = "select * from "+table;
	db.query({
		sql : sqlQuery, 
		callback : callback
	});
}

/**
 * 
 * @param {*} params 
 * @param {*} callback 
 */
const findOne = function(table, values, where, callback){
	var sqlQuery = "SELECT * FROM "+table+" WHERE "+where;
	db.query({
		sql : sqlQuery, 
		values: values,
		callback : callback
	});
}

/**
 * 
 * @param {*} params 
 * @param {*} callback 
 */
const insert = function(values, sqlKeys, sqlValues, table, callback) {	
	var sql = "INSERT INTO "+table+" ("+sqlKeys+") "+"values ("+sqlValues+")";	
	db.query({
		sql : sql, 
		values: values,
		callback : callback
	});
}

/**
 * 
 * @param {*} params 
 * @param {*} callback 
 */
const update = function(values, sqlSet, where, table, callback){
	var sql = "UPDATE "+table+" SET "+sqlSet+" WHERE "+where;
	db.query({
		sql : sql, 
		values: values,
		callback : callback
	});
}

/**
 * 
 * @param {*} params 
 * @param {*} callback 
 */
const deleteRow = function(values, where, table, callback){
	var sqlQuery = "DELETE FROM "+table+" where "+where;
	db.query({
		sql:sqlQuery,
		values:values,
		callback : callback
	});
}

module.exports = {
	insert:insert,
	find:find,
	findOne:findOne,
	update:update,
	deleteRow:deleteRow
}