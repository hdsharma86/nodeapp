-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 24, 2017 at 03:43 PM
-- Server version: 5.7.20-0ubuntu0.17.10.1
-- PHP Version: 7.1.8-1ubuntu1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hapi_todo`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` text NOT NULL,
  `last_modified` datetime NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `summary`, `description`, `meta_keywords`, `meta_description`, `last_modified`, `created_on`) VALUES
(1, 'Proin mattis porttitor dolor', 'Proin mattis porttitor dolor, vitae pulvinar felis pulvinar eget. Donec nec elementum nisl. Aenean neque justo, cursus nec ex ut, volutpat tempus mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus.', 'Proin mattis porttitor dolor, vitae pulvinar felis pulvinar eget. Donec nec elementum nisl. Aenean neque justo, cursus nec ex ut, volutpat tempus mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus.', 'Proin mattis porttitor dolor', 'Proin mattis porttitor dolor', '2017-11-24 00:00:00', '2017-11-24 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `blog_comments`
--

CREATE TABLE `blog_comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `last_modified` datetime NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT '2',
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `address` varchar(250) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `last_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `type_id`, `first_name`, `last_name`, `email`, `password`, `address`, `city`, `state`, `country`, `zip`, `contact`, `last_modified`, `created_on`) VALUES
(1, 2, 'Hardev', 'Sharma', 'test1@test.com', '123456', '#123 Praesent a libero', 'Mohali', 'Punjab', 'IN', '123456', '+91 12345 54321', '2017-11-15 00:00:00', '2017-11-15 00:00:00'),
(2, 2, 'Pancham', 'Singh', 'test2@test.com', '123456', '#123 Praesent a libero', 'Mohali', 'Punjab', 'IN', '123456', '+91 12345 54321', '2017-11-15 00:00:00', '2017-11-15 00:00:00'),
(3, 2, 'Naveen', 'Kumar', 'test3@test.com', '123456', '#123 Praesent a libero', 'Mohali', 'Punjab', 'IN', '123456', '+91 12345 54321', '2017-11-15 00:00:00', '2017-11-15 00:00:00'),
(4, 2, 'Malvinder', 'Singh', 'test4@test.com', '123456', '#123 Praesent a libero', 'Mohali', 'Punjab', 'IN', '123456', '+91 12345 54321', '2017-11-15 00:00:00', '2017-11-15 00:00:00'),
(5, 2, 'Pranab', 'Tiwari', 'test5@test.com', '123456', '#123 Praesent a libero', 'Mohali', 'Punjab', 'IN', '123456', '+91 12345 54321', '2017-11-15 00:00:00', '2017-11-15 00:00:00'),
(7, 2, 'Manoj', 'Tiwari', 'test@test.com', '123456', 'test address', 'Mohali', 'Punjab', 'India', '123456', '1234567890', '2017-11-24 13:25:57', '2017-11-24 13:25:57'),
(8, 2, 'Manoj', 'Tiwari', 'test@test.com', '123456', 'test address', 'Mohali', 'Punjab', 'India', '123456', '1234567890', '2017-11-24 13:27:13', '2017-11-24 13:27:13'),
(9, 2, 'Manoj', 'Tiwari', 'test@test.com', '123456', 'test address', 'Mohali', 'Punjab', 'India', '123456', '1234567890', '2017-11-24 13:27:57', '2017-11-24 13:27:57'),
(10, 2, 'Manoj', 'Tiwari', 'test@test.com', '123456', 'test address', 'Mohali', 'Punjab', 'India', '123456', '1234567890', '2017-11-24 13:32:16', '2017-11-24 13:32:16'),
(11, 2, 'Manoj', 'Tiwari', 'test@test.com', '123456', 'test address', 'Mohali', 'Punjab', 'India', '123456', '1234567890', '2017-11-24 13:32:53', '2017-11-24 13:32:53');

-- --------------------------------------------------------

--
-- Table structure for table `users_type`
--

CREATE TABLE `users_type` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `type_alias` varchar(100) NOT NULL,
  `last_modified` datetime NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_type`
--

INSERT INTO `users_type` (`id`, `type_id`, `type_alias`, `last_modified`, `created_on`) VALUES
(1, 1, 'ADMIN', '2017-11-15 00:00:00', '2017-11-15 00:00:00'),
(2, 2, 'CUSTOMER', '2017-11-15 00:00:00', '2017-11-15 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_comments`
--
ALTER TABLE `blog_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_type`
--
ALTER TABLE `users_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blog_comments`
--
ALTER TABLE `blog_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users_type`
--
ALTER TABLE `users_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
