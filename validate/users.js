
"use strict";

var _ = require('underscore');
var Joi = require('joi');

var UserModel = require('../models/user');

function UserValidate(){};
UserValidate.prototype = (function(){

	return {
		/*findByID: {
			path: (function path() {
				var taskSchema = new models.Task().schema;
				return {
					task_id : taskSchema.taskId.required().rename('taskId')
				};
			})()
		},
		find : {
			query: (function query() {
				var taskSchema = new models.Task().schema;
				return {
					description : taskSchema.description
				};
			})()
		},*/
		insert: {
			payload: (function payload() {
				var userSchema = new UserModel().schema;
				return {
					type_id : userSchema.type_id,
					first_name : userSchema.first_name,
					last_name : userSchema.last_name,
					email : userSchema.email,
					password : userSchema.password,
					address : userSchema.address,
					city : userSchema.city,
					state : userSchema.state,
					country : userSchema.country,
					zip : userSchema.zip,
					contact : userSchema.contact
				};
			})()
		},
		/*update: (function update() {
			var taskSchema = new models.Task().schema;
			return {
				path: {
					task_id : taskSchema.taskId.required().rename('taskId')
				},
				payload: {
					description : taskSchema.description.required()
				}
			}
		})(),
		delete: {
			path: (function path() {
				var taskSchema = new models.Task().schema;
				return {
					task_id : taskSchema.taskId.required().rename('taskId')
				};
			})()
		}*/
	};
})();

var userValidate = new UserValidate();
module.exports = userValidate;