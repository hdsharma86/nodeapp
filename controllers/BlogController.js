const DB_TABLE = 'blog';
const Service = require('./../services/crudService');

/**
 * 
 * @param {*} request 
 * @param {*} callback 
 */
const findBlogs=function(request,callback){
    Service.find(null, DB_TABLE, function(err,data){
        callback(null,data);
    });
}

/**
 * 
 * @param {*} request 
 * @param {*} callback 
 */
const findBlog=function(request, callback){
    var values = [request.params.id];
    var where = "id = ?";
    Service.findOne(DB_TABLE, values, where, function(err,data){
        callback(null,data);
    });
}

module.exports={
    findBlogs:findBlogs,
    findBlog:findBlog
}