const DB_TABLE = 'users';
const Service = require('./../services/crudService');
const addUser = function(payloadData,callback){
    var values = [
		payloadData.type_id,
		payloadData.first_name,
		payloadData.last_name,
		payloadData.email,
		payloadData.password,
		payloadData.address,
		payloadData.city,
		payloadData.state,
		payloadData.country,
		payloadData.zip,
		payloadData.contact
    ];
    var sqlKeys = "type_id,first_name,last_name,email,password,address,city,state,country,zip,contact";
    var sqlValues = "?,?,?,?,?,?,?,?,?,?,?";
    Service.insert(values, sqlKeys, sqlValues, DB_TABLE, function(err,data){
        callback(null,data);
    });
}

/**
 * 
 * @param {*} request 
 * @param {*} callback 
 */
const findUsers=function(request,callback){
    Service.find(null, DB_TABLE, function(err,data){
        callback(null,data);
    });
}

/**
 * 
 * @param {*} request 
 * @param {*} callback 
 */
const findUser=function(request, callback){
    var values = [request.params.user_id];
    var where = "id = ?";
    Service.findOne(DB_TABLE, values, where, function(err,data){
        callback(null,data);
    });
}

/**
 * 
 * @param {*} payloadData 
 * @param {*} DB_TABLE 
 * @param {*} callback 
 */
const updateUser = function(payloadData, callback){
    var values = [
		payloadData.first_name,
		payloadData.last_name,
		payloadData.password,
		payloadData.address,
		payloadData.city,
		payloadData.state,
		payloadData.country,
		payloadData.zip,
		payloadData.contact,
		payloadData.user_id
    ];
    var sqlSet = "first_name = ?, last_name = ?, password = ?, address = ?, city = ?, state = ?, country = ?, zip = ?, contact = ?";
    var where = "id = ?";
    Service.update(values, sqlSet, where, DB_TABLE, function(err, data){
        callback(null,data);
    });
}

/**
 * 
 * @param {*} request 
 * @param {*} DB_TABLE 
 */
const deleteUser=function(request, callback){
    var values = [
		request.params.user_id
    ];
    var where = "id = ?";
    Service.deleteRow(values, where, DB_TABLE, function(err,data){
        callback(null,data);
    });
}

module.exports={
    addUser:addUser,
    findUsers:findUsers,
    findUser:findUser,
    updateUser:updateUser,
    deleteUser:deleteUser
}